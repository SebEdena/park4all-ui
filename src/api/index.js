import mbxClient from "@mapbox/mapbox-sdk";
import mbxDirections from "@mapbox/mapbox-sdk/services/directions";
import mbxGeocoding from "@mapbox/mapbox-sdk/services/geocoding";
import axios from "axios";

/**
 * @class ApiClient
 * @description Class used to interact with the APIs
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
class ApiClient {

    /**
     * Creates an instance of ApiClient.
     * @param {String} mapboxApiKey
     * @param {String} park4allApiUrl
     */
    constructor(mapboxApiKey, park4allApiUrl) {
        this.mapboxApiKey = mapboxApiKey;
        this.mapboxClient = mbxClient({ accessToken: this.mapboxApiKey });
        this.directions = mbxDirections(this.mapboxClient);
        this.geocoding = mbxGeocoding(this.mapboxClient);
        
        this.park4allApiUrl = park4allApiUrl;
        this.park4allClient = axios.create({ 
            baseURL: this.park4allApiUrl
        });
    }

    
    /**
     * @function getPath
     * @description Interface function to call either the mapbox path 
     * or shortest path with parkings
     * @param {Array} start      The starting point
     * @param {Array} finish     The destination point
     * @param {Array} parameters The parameters used for the query
     * @return {Promise} A promise with the result of the call
     */
    getPath = (start, finish, parameters) => {
        if(parameters.parkEnabled){
            return this.getPathApi(start, finish, parameters);
        }else{
            return this.getPathMapbox(start, finish);
        }
    }


    /**
     * @function getPathApi
     * @description Calls the park4all API with parameters
     * @param  {Array} start      The starting point
     * @param  {Array} finish     The destination point
     * @param  {Array} parameters The parameters used for the query
     * @return {Promise} A promise with the result of the call
     */
    getPathApi = (start, finish, parameters) => {
        let startAsString = start.join(",");
        let finishAsString = finish.join(",");

        let priorities = [];
        for(let priority of parameters.priorities) {
            priorities.push(priority.value);
        }
        let prioritiesAsString = priorities.join(",");

        return this.park4allClient.get("/process/path", {
            params: {
                start: startAsString,
                finish: finishAsString,
                vehicle: parameters.vehicle,
                radius: parameters.maxRange,
                priorities: prioritiesAsString
            }
        });
    }

    /**
     * @function getPathMapbox
     * @description Calls the park4all in path-only mode
     * @param  {Array} start      The starting point
     * @param  {Array} finish     The destination point
     * @return {Promise} A promise with the result of the call
     */
    getPathMapbox = (start, finish) => {
        let startAsString = start.join(",");
        let finishAsString = finish.join(",");

        return this.park4allClient.get("/mapbox/path", {
            params: {
                start: startAsString,
                finish: finishAsString
            }
        });
    }

    /**
     * @function searchInput
     * @description Retrieves result to an input for autocompletion
     * @param  {String}       text   The input
     * @param  {LngLatBounds} bounds The geographical boundaries of the query
     * @return {Promise} A promise with the result of the call
     */
    searchInput = (text, bounds) => {
        let flatBounds = [].concat.apply([], bounds.toArray());
        return this.geocoding.forwardGeocode({
            query: text,
            countries: ["fr"],
            bbox: flatBounds,
            limit: 10,
            language: ["fr"],
            types:["postcode","locality","neighborhood","address","poi"]
        }).send();
    }
}

export default ApiClient;