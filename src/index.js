import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';

import App from './components/App';

//Renders the app from the root component, App, at the div #root
ReactDOM.render(
    <App />,
    document.querySelector('#root')
);