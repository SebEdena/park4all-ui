import React from 'react';
import mapboxgl from 'mapbox-gl';

import 'mapbox-gl/dist/mapbox-gl.css';
import './Map.css';

import carparkIcon from "./../assets/img/carpark.svg";
import arrowIcon from "./../assets/img/up-arrow.svg";

const vehicleLabelMapping = {
    car: "Car", 
    motorcycle: "Motorcycle", 
    electric: "Electric car", 
    prm: "PRM"
}

// Options of the zoom animation
const zoomOptions = { 
    padding: {
        top: 100,
        left: 50,
        bottom: 50,
        right: 50
    },
    linear: true,
    duration: 2500
}

/**
 * @class Map
 * @description Map component
 * @extends {React.Component}
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
class Map extends React.Component {

    popups = [];

    state = {
        style: this.props.style,
        ready: false,
        restrictBounds: this.props.restrictBounds,
        mapboxConfig: { apiKey: process.env.REACT_APP_MAPBOX_API_KEY, skinURL: process.env.REACT_APP_MAPBOX_MAP_URL }
    }

    /**
     * @function getOrDefaultInArray
     * @description gets a value at an index in array or a default value
     * @param  {Array}   array        
     * @param  {integer} index        
     * @param  {boolean} fromStart    
     * @param  {*}       defaultValue 
     * @return {*} the value to return
     */
    getOrDefaultInArray = (array, index, fromStart=true, defaultValue) => {
        if(array == null) {
            return defaultValue;
        } else {
            let calcIndex = (fromStart ? index : array.length - 1 - index);
            if(calcIndex < 0 || calcIndex >= array.length){
                return defaultValue;
            } else {
                return array[calcIndex];
            }
        }
    }

    /**
     * @function getOrDefaultInJson
     * @description gets a property in a json object or a default value
     * @param  {type} jsonParentNode
     * @param  {type} property
     * @param  {type} defaultValue
     * @return {type} the value to return
     */
    getOrDefaultInJson(jsonParentNode, property, defaultValue) {
        if(jsonParentNode != null
            && jsonParentNode !== undefined
            && jsonParentNode.hasOwnProperty(property) 
            && jsonParentNode[property] != null 
            && jsonParentNode[property] !== undefined) {
            return jsonParentNode[property];
        } else {
            return defaultValue;
        }
    }

    /**
     * @function canUpdateData
     * @return {boolean} true if the component is ready to update data
     */
    canUpdateData = () => this.state.ready;

    /**
     * @function mustClearPopups
     * @return {boolean} true if the component need to clear popups
     */
    mustClearPopups = () => (this.props.fetching || this.props.route.length === 0);

    /**
     * @function mapNeedsZoom
     * @return {boolean} true if the map needs to perform a zoom
     */
    mapNeedsZoom = () => this.props.zoom;
    
    /**
     * @function componentDidMount
     * @description code to run when the component is mounted
     * @see http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/
     */
    componentDidMount() {
        this.map = new mapboxgl.Map({
            container: this.mapContainer,
            style: this.state.mapboxConfig.skinURL,
            accessToken: this.state.mapboxConfig.apiKey,
            bounds: this.state.restrictBounds
        });

        this.map.once('load', () => {
            // Load carparks icon
            let carpark = new Image();
            carpark.src = carparkIcon;
            carpark.onload = () => this.map.addImage('carpark', carpark);
            
            // Load arrow icon
            let arrow = new Image();
            arrow.src = arrowIcon;
            arrow.onload = () => this.map.addImage('arrow', arrow);

            this.map.addSource("path", { type: "geojson", data: null });
            this.map.addLayer({
                id: "path",
                type: "line",
                source: "path",
                layout: {
                    "line-join": "round",
                    "line-cap": "round"
                },
                paint: {
                    "line-color": "rgba(52, 94, 235, 0.7)",
                    "line-width": 3
                }
            });
            this.map.addLayer({
                id: "path_icons",
                type: "symbol",
                source: "path",
                layout: {
                    'symbol-placement': 'line',
                    'symbol-spacing': 100,
                    'icon-allow-overlap': true,
                    'icon-rotate': 90,
                    'icon-image': 'arrow',
                    'icon-size': 0.9,
                    'visibility': 'visible'
                }
            });

            this.map.addSource("start", { type: "geojson", data: null });
            this.map.addLayer({
                id: "start",
                type: "circle",
                source: "start",
                paint: {
                    "circle-color": "rgb(36, 143, 34)",
                    "circle-radius": 6,
                    "circle-stroke-color": "white",
                    "circle-stroke-width": 2
                }
            });

            this.map.addSource("finish", { type: "geojson", data: null });
            this.map.addLayer({
                id: "finish",
                type: "circle",
                source: "finish",
                paint: {
                    "circle-color": "rgb(217, 29, 20)",
                    "circle-radius": 6,
                    "circle-stroke-color": "white",
                    "circle-stroke-width": 2
                }
            });

            this.map.addSource("carpark", { type: "geojson", data: null });
            this.map.addLayer({
                id: "carpark",
                type: "symbol",
                source: "carpark",
                layout: {
                    'icon-image': 'carpark',
                    'icon-allow-overlap': true
                }
            });
            
            // Code based on https://docs.mapbox.com/mapbox-gl-js/example/popup-on-click/
            this.map.on('click', 'carpark', (e) => {
                let coordinates = e.features[0].geometry.coordinates.slice();
                let html = e.features[0].properties.html;
                 
                // Ensure that if the map is zoomed out such that multiple
                // copies of the feature are visible, the popup appears
                // over the copy being pointed to.
                while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                }
                 
                let popup = new mapboxgl.Popup()
                    .setLngLat(coordinates)
                    .setHTML(html)
                    .addTo(this.map);
                this.popups.push(popup);
            });

            // Code based on https://docs.mapbox.com/mapbox-gl-js/example/popup-on-click/
            // Change the cursor to a pointer when the mouse is over the places layer.
            this.map.on('mouseenter', 'carpark', () => {
                this.map.getCanvas().style.cursor = 'pointer';
            });
             
            // Code based on https://docs.mapbox.com/mapbox-gl-js/example/popup-on-click/
            // Change it back to a pointer when it leaves.
            this.map.on('mouseleave', 'carpark', () => {
                this.map.getCanvas().style.cursor = '';
            });

            this.setState({ ready: true });
        });
    }

    /**
     * @function componentDidUpdate
     * @description code to run when the component has been updated
     * @see http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/
     */
    componentDidUpdate() {        
        if(this.canUpdateData()) {
            if(this.mustClearPopups()) this.closeAndDestroyAllPopups();
            this.map.getSource("path").setData({
                type: "Feature",
                properties: {},
                geometry: {
                    type: "LineString",
                    coordinates: this.props.route
                }
            });
            this.map.getSource("start").setData({
                type: "Point",
                coordinates: this.getOrDefaultInArray(this.props.route, 0, true, this.props.start)
            });
            this.map.getSource("finish").setData({
                type: "Point",
                coordinates: this.getOrDefaultInArray(this.props.route, 0, false, this.props.finish)
            });
            this.map.getSource("carpark").setData(this.parkingFeatures());
            if(this.mapNeedsZoom()) this.updateZoom();
        }
    }

    /**
     * @function componentWillUnmount
     * @description code to run when the component will be destroyed
     * @see http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/
     */
    componentWillUnmount() {
        this.map.remove();
    }

    /**
     * @function updateZoom
     * @description updates the zoom of the map to fit the route
     */
    updateZoom = () => {
        // Code from https://docs.mapbox.com/mapbox-gl-js/example/zoomto-linestring/
        let coordinates = this.props.route;

        if(coordinates.length > 0) {

            let bounds = coordinates.reduce(function (bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
    
            this.map.fitBounds(bounds, zoomOptions);

        } else {
            this.map.fitBounds(this.props.restrictBounds);
        }
        this.props.zoomDone();
    }

    /**
     * @function parkingFeatures
     * @description initializes the parking features object to display car parks
     * @return {Object}
     */
    parkingFeatures = () => {
        let value = {
            type: 'FeatureCollection',
            features: []
        };
        
        let parkCount = 0;
        for (let carpark of this.props.carparks) {
            let parkData = this.getOrDefaultInJson(carpark, "park", null);
            if(parkData != null) {
                parkCount++;
                let feature = {
                    type: 'Feature',
                    properties: {
                        html: this.buildHTMLTemplate(carpark, this.props.currentVehicle, parkCount)
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: [parkData.geo.longitude, parkData.geo.latitude]
                    }
                };
                value.features.push(feature);
            }
        }

        return value;
    }

    /**
     * @function buildHTMLTemplate
     * @description builds the html code for the popup
     * @param  {Object}  carpark   the car park data
     * @param  {String}  vehicle   the type of vehicle
     * @param  {integer} parkCount the id of the car park on the path
     * @return {type} {description}
     */
    buildHTMLTemplate(carpark, vehicle, parkCount) {
        let parkName = this.getOrDefaultInJson(carpark.park, "parkName", "Unknown park name");
        let type = this.getOrDefaultInJson(carpark.park, "type", "UNKNOWN");
        let address = this.getOrDefaultInJson(carpark.park, "address", "Unknown address");
        let price = this.getOrDefaultInJson(carpark, "price", null);
        let vehicleLabel = vehicleLabelMapping[vehicle];
        let specificPlaces = this.getOrDefaultInJson(carpark, "specificPlaces", 0);
        let alternativePlaces = this.getOrDefaultInJson(carpark, "alternativePlaces", 0);
        return (
            `
            <div>
                <p>
                    <b>${parkCount}) ${parkName}</b><br/>
                    ${type === "PUBLIC" ? "" : address + "<br/>"}
                    Company: ${type}<br/>
                    ${price == null ? 'Unknown price' : `Price: ${price} €/hour`}<br/>
                    ${vehicleLabel} place(s): ${specificPlaces}<br/>
                    Other place(s): ${alternativePlaces}
                </p>
            </div>
            `
        );
    }

    /**
     * @function closeAndDestroyAllPopups
     * @description safe removal of popups
     */
    closeAndDestroyAllPopups() {
        for(let popup of this.popups) {
            popup.remove();
        }
        this.popups = [];
    }

    /**
     * @function render
     * @description Renders the component
     * @return {JSX} The React component display
     */
    render() {
        return (
            <div className="map" style={this.state.style} ref={el => (this.mapContainer = el)} />
        );
    }

}

export default Map;