import React from 'react';

import { Form } from 'semantic-ui-react';

import SearchBar from './SearchBar';

const style = {
    width: "100%",
    padding: "8px",
    borderRadius: "5px",
    backgroundColor: "rgba(52, 52, 52, 0.5)",
}

const buttonDivStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
}

const searchBarStyle = {
    display: "flex",
    alignItems: "center"
}

const labelStyle = {
    color: "white"
}

const initialValueState = { text: "", data: null }

/**
 * @class SearchForm
 * @description Search Form component
 * @extends {React.Component}
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
class SearchForm extends React.Component {

    state = {
        start: initialValueState,
        finish: initialValueState
    }

    
    /**
     * Creates an instance of SearchForm.
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.mapboxClient = this.props.mapboxClient;
    }
    
    /**
     * @function clearSearch
     * @description Clears the current search
     * @param  {Event} event
     */
    clearSearch = event => {
        event.preventDefault();
        this.setState({
            start: initialValueState,
            finish: initialValueState
        });
        this.props.clear();
    }

    /**
     * @function onFormSubmit
     * @param  {Event} event
     */
    onFormSubmit = event => {
        event.preventDefault();
        if(this.state.start.data == null || this.state.finish.data == null){
            this.props.toggleModalError("noDataForm");
        }else{
            this.props.search(this.state.start.data.center, this.state.finish.data.center);
        }
    }

    /**
     * @function render
     * @description Renders the component
     * @return {JSX} The React component display
     */
    render() {
        return (
            <Form className="search-form" style={style} onSubmit={e => this.onFormSubmit(e)}>
                <Form.Group style={{ margin: 0 }} widths="equal">
                    <Form.Field
                        inline
                        style={searchBarStyle}>
                        <label style={labelStyle}>From</label>
                        <SearchBar value={this.state.start} restrictBounds={this.props.restrictBounds}
                            updateValue={(value, data) => {this.setState({ start: { text: value, data: data} }) }}
                        />
                    </Form.Field>
                    <Form.Field
                        inline
                        style={searchBarStyle}>
                        <label style={labelStyle}>To</label>
                        <SearchBar value={this.state.finish} restrictBounds={this.props.restrictBounds}
                            updateValue={(value, data) => {this.setState({ finish: { text: value, data: data} }) }}
                        />
                    </Form.Field>
                    <div style={buttonDivStyle}>
                        <Form.Button onClick={e => this.clearSearch(e)}>Clear</Form.Button>
                        <Form.Button onClick={e => this.onFormSubmit(e)} color='green'>Search</Form.Button>
                    </div>
                </Form.Group>
            </Form>
        );
    }
};

export default SearchForm;