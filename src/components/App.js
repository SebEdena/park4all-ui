import React from 'react';

import ApiClient from '../api/';
import { LngLatBounds, LngLat } from 'mapbox-gl';

import Map from './Map';
import SearchForm from './SearchForm';

import { Button, Icon, Dimmer, Loader } from 'semantic-ui-react';
import SettingsModal from './SettingsModal';
import ErrorModal from './ErrorModal';

const mapStyle = {
    position: 'absolute',
    top: 0,
    bottom: 0,
    width: '100%'
};

const appOverlayStyle = {
    position: 'absolute',
    padding: "10px",
    width: "100%",
    display: "flex",
    alignItems: "baseline"
};

const settingButtonStyle = {
    backgroundColor: "rgba(52, 52, 52, 0.5)"
};

// Settings of the distance gauge
const distanceSettings = {
    min: 50,
    max: 500,
    unitAdd: 50
};

/**
 * @class App
 * @description Core component of the application
 * @extends {React.Component}
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
class App extends React.Component {

    state = {
        modalOpened: false,
        modalError: false,
        errorText: null,
        restrictBounds: new LngLatBounds(
            new LngLat(2.186, 48.809),
            new LngLat(2.490, 48.915)
        ),
        start: {},
        finish: {},
        route: [],
        carparks: [],
        currentVehicle: "car",
        zoom: false,
        parameters: {
            parkEnabled: true,
            vehicle: "car",
            maxRange: 500,
            priorities: [
                {value: "distance", label: "Distance"}, 
                {value: "price", label: "Price"}
            ]
        },
        loading: false
    }

    /**
     * Creates an instance of App
     * @param {*} props
     */
    constructor(props){
        super(props);
        this.apiClient = new ApiClient(
            process.env.REACT_APP_MAPBOX_API_KEY, 
            process.env.REACT_APP_PARK4ALL_API_URL 
        );
    }

    /**
     * @function toggleModal
     * @description Switches the settings modal on/off
     */
    toggleModal = () => {
        this.setState({modalOpened: !this.state.modalOpened})
    }

    /**
     * @function toggleModalError
     * @description Switches the error modal on/off
     * @param {String} value The key of the error
     * @see ErrorModal.js
     */
    toggleModalError = (value) => {
        this.setState({modalError: !this.state.modalError, errorText: value})
    }

    /**
     * @function zoomDone
     * @description Sets the zoom state as false
     */
    zoomDone = () => {
        this.setState({zoom: false});
    }

    /**
     * @function priorityUpdate
     * @description changes the priority order (distance, price)
     * @param {integer} index the index to change
     * @param {boolean} up    push it up if true, down otherwise
     */
    priorityUpdate = (index, up) => {
        if(this.state.parameters.priorities.length > 1) {
            if(up && index !== 0) {
                this.setState((prevState) => {
                    let newPriorities = prevState.parameters.priorities;
    
                    let tmp = newPriorities[index];
                    newPriorities[index] = newPriorities[index - 1];
                    newPriorities[index - 1] = tmp;
    
                    prevState.parameters.priorities = newPriorities;
                    return prevState;
                });
            } else if(!up && index !== this.state.parameters.priorities.length - 1) {
                this.setState((prevState) => {
                    let newPriorities = prevState.parameters.priorities;
    
                    let tmp = newPriorities[index];
                    newPriorities[index] = newPriorities[index + 1];
                    newPriorities[index + 1] = tmp;
    
                    prevState.parameters.priorities = newPriorities;
                    return prevState;
                });
            }
        }
    }

    /**
     * @function isLoading
     * @description Switches the loading property on/off
     */
    isLoading = () => {
        this.setState({ loading: !this.state.loading });
    }

    /**
     * @function search
     * @description launches a search query
     * @param {Array} start  
     * @param {Array} finish 
     */
    search = (start, finish) => {
        this.isLoading();
        this.apiClient.getPath(start, finish, this.state.parameters)
            .then(res => {
                console.log(res);
                
                let data = res.data;
                if(data.result.routes.length < 1){
                    this.toggleModalError("noResultError");
                    this.isLoading();
                } else {
                    this.setState({ 
                        start: start, 
                        finish: finish, 
                        route: data.result.routes[0].geometry.coordinates,
                        carparks: data.carparks,
                        currentVehicle: data.param.vehicle, 
                        zoom:true
                    });
                    this.isLoading();
                }
            })
            .catch(err => {
                this.isLoading();
                this.toggleModalError("serverUnavaibleError");
            });
    }

    /**
     * @function clear
     * @description clears the current data
     */
    clear = () => {
        this.setState({start : {}, finish : {}, route : [], carparks: [], zoom:true });
    }

    /**
     * @function render
     * @description Renders the component
     * @return {JSX} The React component display
     */
    render() {
        const { loading } = this.state;
        return (
            <div id="app-wrapper">
                <Dimmer active={loading} page={true}>
                    <Loader size='large' inline='centered'>Loading your path, please wait</Loader>
                </Dimmer>
                <ErrorModal toggleFn={this.toggleModalError} opened={this.state.modalError} errorText={this.state.errorText}/>
                <Map 
                    style={mapStyle} 
                    restrictBounds={this.state.restrictBounds} 
                    start={this.state.start}
                    finish={this.state.finish}
                    route={this.state.route}
                    carparks={this.state.carparks}
                    currentVehicle={this.state.currentVehicle}
                    fetching={this.state.loading}
                    zoom={this.state.zoom}
                    zoomDone={() => this.zoomDone()}
                    modalOpened={this.state.modalOpened}
                />
                <div id="app-overlay" style={appOverlayStyle}>
                    <Button style={settingButtonStyle} onClick={this.toggleModal} icon color="grey" labelPosition='left'>
                        <Icon name="setting" /> Settings
                    </Button>
                    <SearchForm search={this.search} restrictBounds={this.state.restrictBounds} clear={this.clear} toggleModalError={this.toggleModalError}/>
                </div>
                <SettingsModal
                    opened={this.state.modalOpened}
                    toggleFn={this.toggleModal}
                    parameters={this.state.parameters}
                    distanceSettings={distanceSettings}
                    enablePark={value => this.setState({ parameters: { ...this.state.parameters, parkEnabled: value }})}
                    onVehicleSelect={value => this.setState({ parameters: { ...this.state.parameters, vehicle: value }})}
                    onProgressChange={value => this.setState({ parameters: { ...this.state.parameters, maxRange: value }})}
                    onPriorityUpdate={(index, up) => this.priorityUpdate(index, up)}
                />
            </div>
        );
    }
};

export default App;