import React from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';

import ApiClient from '../api';

import "./SearchBar.css";

import { Search } from 'semantic-ui-react';

const inputStyle = {
    flexGrow: 1,
    display: "flex",
    width: "100%"
}

// Method to render a result on the search list
const resultRenderer = ({ id, place_name }) => <div key={id}><b>{place_name}</b></div>;

resultRenderer.propTypes = {
    place_name: PropTypes.string,
    id: PropTypes.string
}

const initialState = { isLoading: false, results: null }

/**
 * @class SearchBar
 * @description Search Bar component
 * @extends {React.Component}
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
class SearchBar extends React.Component {

    state = initialState;

    /**
     * Creates an instance of SearchBar.
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.mapboxClient = new ApiClient(process.env.REACT_APP_MAPBOX_API_KEY);
    }

    /**
     * @function filterResult
     * @description filters data only in Paris (75)
     * @param  {Array} data
     * @return {Array} The filtered data
     */
    filterResult = (data) => {
        return data.filter(element => {
            if (element.id.startsWith("postcode")) {
                return element.text.startsWith("75");
            } else {
                for (let elt of element.context) {
                    if (elt.id.startsWith("postcode")) {
                        return elt.text.startsWith("75");
                    }
                }
                return false;
            }
        });
    }

    /**
     * @function onResultSelect
     * @param  {Event} e        
     * @param  {Object} result
     */
    onResultSelect = (e, { result }) => { this.props.updateValue(result.place_name, result) };

    /**
     * @function onSearchChange
     * @param  {Event}  e 
     * @param  {String} value the selected value
     */
    onSearchChange = (e, { value }) => {
        this.props.updateValue(value, null);
        if (value !== "") {
            this.setState({ isLoading: true });
            this.mapboxClient.searchInput(value, this.props.restrictBounds)
                .then(res => {
                    let filteredResults = this.filterResult(res.body.features);
                    this.setState({
                        isLoading: false,
                        results: filteredResults,
                    });
                })
                .catch(err => console.error(err));
        }
    }

    /**
     * @function render
     * @description Renders the component
     * @return {JSX} The React component display
     */
    render() {
        const { isLoading, results } = this.state;

        return (
            <Search
                fluid
                style={inputStyle}
                loading={isLoading}
                onResultSelect={this.onResultSelect}
                onSearchChange={debounce(this.onSearchChange, 500, {leading: true})}
                resultRenderer={resultRenderer}
                results={results}
                value={this.props.value.text}
                placeholder='Search...'
            />
        );
    }
};

export default SearchBar;