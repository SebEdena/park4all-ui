import React from 'react';

import {
    Button,
    Header,
    Icon,
    Modal,
    Grid,
    Checkbox,
    Radio,
    Form,
    Progress,
    List,
    Divider
} from 'semantic-ui-react';

import "./SettingsModal.css";

const progressButtonsDiv = {
    display: "flex",
    justifyContent: "center"
}

/**
 * @class SettingsModal
 * @description Setting Modal 
 * @extends {React.Component}
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
class SettingsModal extends React.Component {
    
    /**
     * Creates an instance of SettingsModal.
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.progressRef = React.createRef();
    }

    /**
     * @function enablePark
     * @param  {Event}   e 
     * @param  {boolean} checked true if enable
     */
    enablePark = (e, { checked }) => { this.props.enablePark(checked) }

    /**
     * @function progressChange
     * @description updates the parameters for the radius progress bar
     * @param {boolean} add true if addition
     */
    progressChange = add => {
        if (add) {
            this.props.onProgressChange(
                Math.min(
                    this.props.parameters.maxRange + this.props.distanceSettings.unitAdd,
                    this.props.distanceSettings.max
                ));
        } else {
            this.props.onProgressChange(
                Math.max(
                    this.props.parameters.maxRange - this.props.distanceSettings.unitAdd,
                    this.props.distanceSettings.min
                ));
        }
    }

    /**
     * @function onVehicleSelect
     * @param  {Event}  e 
     * @param  {String} value
     */
    onVehicleSelect = (e, { value }) => this.props.onVehicleSelect(value);

    /**
     * @function onPriorityUpdate
     * @description fires the update of priorities
     * @param  {integer} index
     * @param  {boolean} up true if move priority up
     * @see App.js
     */
    onPriorityUpdate = (index, up) => this.props.onPriorityUpdate(index, up);

    /**
     * @function toggle
     * @description switches on/off the modal
     */
    toggle = () => this.props.toggleFn();

    /**
     * @function render
     * @description Renders the component
     * @return {JSX} The React component display
     */
    render() {
        let priorityList = this.props.parameters.priorities.map((elt, index) => {
            return (
                <List.Item key={elt.value}>
                    <List.Content floated='right'>
                        <Button 
                            icon='angle up' 
                            onClick={() => this.onPriorityUpdate(index, true)} 
                            inverted 
                            disabled={index===0 || !this.props.parameters.parkEnabled} />
                        <Button 
                            icon='angle down' 
                            onClick={() => this.onPriorityUpdate(index, false)} 
                            inverted 
                            disabled={index === 
                                (this.props.parameters.priorities.length - 1)
                                || !this.props.parameters.parkEnabled}/>
                    </List.Content>
                    <List.Content>
                        <Header as='h4' disabled={!this.props.parameters.parkEnabled}>{index + 1}</Header>
                        <Header as='h4' disabled={!this.props.parameters.parkEnabled}>{elt.label}</Header>
                    </List.Content>
                </List.Item>
            );
        });

        return (
            <Modal
                open={this.props.opened}
                basic
                closeOnDimmerClick={true}
                size='large'>
                <Header icon='cog' content='Search Settings' />
                <Modal.Content>
                    <Checkbox toggle label="Enable carpark search"
                        checked={this.props.parameters.parkEnabled}
                        onClick={this.enablePark}
                    />
                    <Divider inverted />
                    <Grid columns={2} divided inverted>
                        <Grid.Row>
                            <Grid.Column>
                                <Form inverted>
                                    <Header inverted as='h5' disabled={!this.props.parameters.parkEnabled}>Your vehicle</Header>
                                    <Form.Field disabled={!this.props.parameters.parkEnabled}>
                                        <Radio
                                            label='Car'
                                            name='vehicleSelector'
                                            value='car'
                                            checked={this.props.parameters.vehicle === 'car'}
                                            onChange={this.onVehicleSelect}
                                        />
                                    </Form.Field>
                                    <Form.Field disabled={!this.props.parameters.parkEnabled}>
                                        <Radio
                                            label='Motorcycle'
                                            name='vehicleSelector'
                                            value='motorcycle'
                                            checked={this.props.parameters.vehicle === 'motorcycle'}
                                            onChange={this.onVehicleSelect}
                                        />
                                    </Form.Field>
                                    <Form.Field disabled={!this.props.parameters.parkEnabled}>
                                        <Radio
                                            label='Electric car'
                                            name='vehicleSelector'
                                            value='electric'
                                            checked={this.props.parameters.vehicle === 'electric'}
                                            onChange={this.onVehicleSelect}
                                        />
                                    </Form.Field>
                                    <Form.Field disabled={!this.props.parameters.parkEnabled}>
                                        <Radio
                                            label='People with Reduced Mobility'
                                            name='vehicleSelector'
                                            value='prm'
                                            checked={this.props.parameters.vehicle === 'prm'}
                                            onChange={this.onVehicleSelect}
                                        />
                                    </Form.Field>
                                    <Divider inverted />
                                    <Form.Field disabled={!this.props.parameters.parkEnabled}>
                                        <Progress
                                            ref={this.progressRef}
                                            value={this.props.parameters.maxRange}
                                            total={this.props.distanceSettings.max}
                                            label="Distance of carpark search (meters)"
                                            color='black'
                                            progress='value'
                                            inverted />
                                        <div style={progressButtonsDiv}>
                                            <Button size="tiny" icon='minus' onClick={() => this.progressChange(false)} inverted color="red" />
                                            <Button size="tiny" icon='plus' onClick={() => this.progressChange(true)} inverted color="green" />
                                        </div>
                                    </Form.Field>
                                </Form>
                            </Grid.Column>
                            <Grid.Column>
                                <Header inverted as='h5' disabled={!this.props.parameters.parkEnabled}>Your priorites</Header>
                                <List divided inverted verticalAlign='middle'>
                                    {priorityList}
                                </List>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='green' onClick={this.toggle} inverted>
                        <Icon name='checkmark' /> Confirm
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
};

export default SettingsModal;