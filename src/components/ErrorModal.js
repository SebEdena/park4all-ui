import React from 'react';
import {
    Header,
    Modal,
    Button, 
    Icon
} from 'semantic-ui-react';

const textError = {
    serverUnavaibleError : {
        text: "API Server is unavailable.",
        title: "Network Error"
    },
    noResultError: {
        text: "No results between Start and Finish",
        title: "Search Error"
    },
    unknownError: {
        text: "Unknown Error",
        title: "Error"
    },
    noDataForm: {
        text: "You have not selected values in the form",
        title: "Form Error"
    }
}

/**
 * @class ErrorModal
 * @description Error Modal
 * @extends {React.Component}
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
class ErrorModal extends React.Component{

    /**
     * @function toggle
     * @description switches on/off the modal
     */
    toggle = () => this.props.toggleFn();

    /**
     * @function render
     * @description Renders the component
     * @return {JSX} The React component display
     */
    render () {
        let error = textError.hasOwnProperty(this.props.errorText)? textError[this.props.errorText] : textError["unknownError"];
        return (
            <Modal
                open={this.props.opened}
                basic
                closeOnDimmerClick={true}
                size='large'>
                <Header icon='exclamation triangle' content={error.title} />
                <Modal.Content>
                    <h1>{error.text}</h1>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={this.toggle} inverted>
                        <Icon name='checkmark' /> Confirm
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default ErrorModal;