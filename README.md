# park4all-ui
UI for the Park4All project that provides navigation with parking settings

## Windows setup
### Prerequisites
- [Node.js](https://nodejs.org/en/) latest LTS version
- [Git CLI (bash)](https://git-scm.com/) latest release
- [Mapbox Account](https://www.mapbox.com/) with an access token
- [Park4All API](https://gitlab.com/SebEdena/park4all-api) ready

### Setup
On the destination folder in CLI, run `git clone https://gitlab.com/SebEdena/park4all-ui.git`

Open the folder `park4all-ui`. Duplicate file `.env.template` and rename copy as `.env`. Insert your Mapbox access token to replace the string `<Your Mapbox Token here>` for the key `REACT_APP_MAPBOX_API_KEY` and save your modifications.

Inside CLI, run :
`npm install`
`npm start`

The browser will open in **development** mode.